var app = {};

var	async = require('async');
var redis = require("redis");

var host = '172.16.18.63';
var port = 6380;
    

// if you'd like to select database 3, instead of 0 (default), call
// client.select(3, function() { /* ... */ });

app.setReserve = function(aid, gameid, roomid, seatid){
	var key = 'reserveG'+gameid+'R'+roomid+'S'+seatid;
	var value = {
		aid: aid,
		expired: Date()
	};

	var client = redis.createClient({host:host, port:port});

	client.on("error", function (err) {
	    console.log("Error " + err);
	});

	async.series([
		function(cb){
			client.set([key, JSON.stringify(value)], redis.print);
			// console.log(redis.print);
			cb();
		},
		function(cb){
			client.get(key, function(err, reply){
				console.log(reply);
				cb();
			});
		}

	],
	function(err, results){
		if(err) console.log(err);
		client.quit();
	});
}

app.subQueries = function(){
	var client = redis.createClient({host:host, port:port});

	async.series([
		function(cb){
			client.keys('*', function (err, keys) {
			    keys.forEach(function (key, pos) {
			        client.get(key, function (err, reply) {
			            console.log(key + ' is ' + reply);
			            if (pos === (keys.length - 1)) {
			                cb();
			            }
			        });
			    });
			});
		}
	],
	function(err, results){
		if(err) console.log(err);
		client.quit();
	});
	
}

module.exports = app;