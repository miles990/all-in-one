#!/usr/bin/env node

var program = require('commander');

program
	.version('0.0.1')
	.usage('[options]')
	.option('-c, --connect-redis', '跟 redis server 連線')
	.option('-r, --reset-lobby', '重設 lobby 相關 db 資料 ( mongodb )')
	// .option('-g, --lobby-gamelist', '模擬 [client -> agent] lobby.gamelist.req')
	// .option('-G, --lobby-gamelist2', '模擬 [agent -> service] lobby.gamelist.req')
	// .option('-s, --send-to-queue', '送 bon 到 mq')
	// .option('-r, --receive-from-queue', '從 mq 收 bson')
	// .option('-i, --interactive', '互動模式')
	// .option('-a2s, --agent2service', '模擬 agent -> service')
	// .option('-s2s, --service2service', '模擬 service -> service')
	// .option('-s2a, --service2agent', '模擬 service -> agent')
	.description('Redis 工具: \n\t1. 建立保留座資料 \n*\t2. 重設 db 資料')
	.parse(process.argv);

function help(){
	var stdin =  'node '+ process.mainModule.filename + ' --help';
	var child_process = require('child_process');
	child_process.exec(stdin, function(err, stdout, stderr){
		if(err) throw err;
		console.log(stdout);
		process.exit();
	});
}

function connectRedis(){
	var redisclient = require('./redisclient');
	// redisclient.setReserve('testaccount', 7, 2, 10);
	redisclient.subQueries();
}

function resetLobby(){
	var mongoclient = require('./mongoclient');
}

if(program.connectRedis){
	connectRedis(program);
}else if(program.resetLobby){
	resetLobby(program);
}else{
	help();
}

// main();