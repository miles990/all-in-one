var app = {};
module.exports = app;

var Tools = require('./tools.js');
var async = require('async');
var ObjectID = require('mongodb').ObjectID;
var uuid = require('node-uuid');

// app.host = '172.16.18.63';
// app.host = '172.16.210.120';
app.host = 'localhost';
//app.host = 'mongodb'
app.user = 'mongo_admin'
app.password = '827ccb0eea8a706c4c34a16891f84e7b'
app.port = 27017;


app.resetGamelistData = function(callback){
	var MongoClient = require('mongodb').MongoClient;
	var Server = require('mongodb').Server
	var dbName = 'casino_game';

	var collectionName = 'gamelist';
	var doc = {
		// _id: new ObjectID(parseInt(1)),
		data: Tools.loadJSONfile(dbName+'/'+collectionName+'.json')
	};

	// Connection URL
	var url = 'mongodb://'+app.user+':'+app.password+'@'+app.host+':'+app.port+'/'+dbName+'?authMechanism=SCRAM-SHA-1';

	console.log(url)
	var dbConnection = null;
	async.series([
		function(cb){
			MongoClient.connect(url, function(err, db) {
				dbConnection = db;
				cb(err, true);
			});
		},	
		function(cb){
		 	dbConnection.dropCollection(collectionName, function(err, r) {
		 		cb(err, true);
		 	});
		 },
		function(cb){
			dbConnection.collection(collectionName).insertOne(doc, {w:1}, function(err, r) {
				console.log(r)
				cb(err, true);
			});
		}
	],
	function(err, results){
		if(err) console.log(err);
		dbConnection.close();
		console.log('resetGamelistData success !!');
		if(callback) callback();
	});
}

app.findGamelist = function(callback){
	var MongoClient = require('mongodb').MongoClient;
	var dbName = 'casino_game';

	var collectionName = 'gamelist';
	
	// Connection URL
	var url = 'mongodb://'+app.user+':'+app.password+'@'+app.host+':'+app.port+'/'+dbName;

	var dbConnection = null;
	var list = [];
	async.series([
		function(cb){
			MongoClient.connect(url, function(err, db) {
				dbConnection = db;
				cb(err, true);
			});
		},
		function(cb){
			dbConnection.collection(collectionName).findOne(function(err, docs) {
				var gamelist = docs["data"];
				for(var i = 0; i < gamelist.length; i++){
					var gameid = gamelist[i].id;
					list.push(gameid);
				}
				cb(err, list);
			});
		}
	],
	function(err, results){
		if(err) console.log(err);
		dbConnection.close();
		var result = results.pop();
		console.log('gamelist:',result);

		if(callback) callback(result);
	});
}


function main(){
	async.series([
		// casino_game
		function(cb){ // gamelist 資料
			app.resetGamelistData(function(){
				cb();
			});
		},
		
	],
	function(err, results){
		if(err) console.log(err);
	});
}

main();
