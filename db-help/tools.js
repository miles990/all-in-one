var app = {};

// const EventEmitter = require('events');
// app.handler = new EventEmitter();

app.loadJSONfile = function(path){
	// console.log(path);
	var fs = require('fs');
	var readfile = fs.readFileSync(path);
	var doc = JSON.parse(readfile);
	return doc;
}

app.bsonSerialize = function(data){
	var bson = require('bson');
	var BSON = new bson.BSONPure.BSON();
	var Long = bson.BSONPure.Long;
	var bsonData = BSON.serialize(data, false, true, false);
	
	return bsonData;
}

// app.sendDisconnectEvent = function(){
// 	// console.log(' # Tools sendDisconnectEvent')
// 	app.handler.emit('disconnect');
// }

module.exports = app;